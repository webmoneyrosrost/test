<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'email'     => 'required|string|max:50',
            'password'  => 'required|max:50',
        ];
    }
}
