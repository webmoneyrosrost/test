<?php

namespace App\Http\Requests\Post;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'title'         => 'required|string|min:3|max:50',
            'description'   => 'required|string|min:3|max:350',
        ];
    }

}
