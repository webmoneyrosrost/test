<?php

namespace App\Http\Controllers;
use App\Events\PostCreated;
use App\Events\PostDeleted;
use App\Events\PostUpdated;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Cache::remember('allPosts', 3600, function() {
            return Post::all();
        });

        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);

        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $this->authorize('create', Post::class);

        $newPost = Post::create($request->validated());

        event(new PostCreated($newPost));

        return response()->json([
            'status'    => 'success',
            'message'   => 'Post has been created successfully',
            'route'     => route('posts.index'),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $this->authorize('update', Post::class);

        return view('post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $this->authorize('update', Post::class);

        $input = $request->validated();
        $post->title        = $input['title'];
        $post->description  = $input['description'];
        $post->save();

        event(new PostUpdated($post));

        return response()->json([
            'status'    => 'success',
            'message'   => 'Post has been updated successfully',
            'route'     => route('posts.index'),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', Post::class);

        event(new PostDeleted($post));

        $post->delete();
        return response()->json([
            'status'    => 'success',
            'message'   => 'Post has been deleted successfully',
        ], 200);

    }
}
