<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Http\Requests\Post\UpdateRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Cache::remember('allPosts', 3600, function() {
            return Post::all();
        });
        return response()->json([
            "success"   => true,
            "message"   => "Post List",
            "data"      => $posts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $this->authorize('create', Post::class);

        $input = $request->validated();

        $post = Post::create($input);

        return response()->json([
            "success"   => true,
            "message"   => "Post created successfully.",
            "data"      => $post
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return response()->json([
            "success"   => true,
            "message"   => "Post retrieved successfully.",
            "data"      => $post
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param
     * @param
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Post $post)
    {
        $this->authorize('update', Post::class);

        $input = $request->validated();

        $post->title        = $input['title'];
        $post->description  = $input['description'];
        $post->save();

        return response()->json([
            "success"   => true,
            "message"   => "Post updated successfully.",
            "data"      => $post
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', Post::class);
        $post->delete();

        return response()->json([
            "success"   => true,
            "message"   => "Post deleted successfully.",
            "data"      => $post
        ]);
    }
}
