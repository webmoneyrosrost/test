<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\LoginRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function loginAuth(LoginRequest $request)
    {
        if (Auth::attempt($request->validated())) {
            return response()->json([
                'status'    => 'success',
                'route'     => route('posts.index'),
            ], 200);
        } else {
            return response()->json([
                'error'     => 'Invalid user data',
            ], 400);
        }
    }

    public function logout()
    {
        Auth::check() ?: Auth::logout();

        return response()->json([
            'status'    => 'success',
            'message'   => 'Logout successfully',
            'route'     => route('login'),
        ], 200);
    }
}
