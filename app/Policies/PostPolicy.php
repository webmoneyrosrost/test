<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user
     * @return bool
     */
    public function update(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine if the given post can be updated by the user.
     *
     * @param \App\Models\User $user
     * @return bool
     */
    public function delete(User $user)
    {
        return $user->is_admin;
    }

}
