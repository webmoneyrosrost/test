# Проект test
----------------
### Как запустить у себя локально, что нужно установить и какие команды для запуска выполнить

Чтобы запустить локально необходим docker. docker-compose. инструкции по установке 
###### docker
- Debian (linux) https://docs.docker.com/install/linux/docker-ce/debian/
- Centos (linux) https://docs.docker.com/install/linux/docker-ce/centos/
- Mac https://docs.docker.com/docker-for-mac/install/
- Windows https://docs.docker.com/docker-for-windows/install/

###### docker-compose
- Выбрать подходящую ОС https://docs.docker.com/compose/install/#install-compose 

###### .env
- Сделайте из .env.example

###### Поднять проект
В корне проекта перейти в папку **docker/** и выполнить команды:

```php
docker-compose -f docker-compose-dev.yml build
docker-compose -f docker-compose-dev.yml up -d
docker-compose -f docker-compose-dev.yml run artisan migrate
docker-compose -f docker-compose-dev.yml run artisan db:seed --class UserSeeder
docker-compose -f docker-compose-dev.yml run artisan db:seed --class PostSeeder
```

В файле hosts прописать: 127.0.0.1       test.loc

Через браузер зайти на http://test:89/ 


