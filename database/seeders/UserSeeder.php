<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'      => 'admin',
            'email'     => 'admin@test.loc',
            'password'  => Hash::make('passwordA'),
            'is_admin'  => true
        ]);

        $user = User::create([
            'name'      => 'user',
            'email'     => 'user@test.loc',
            'password'  => Hash::make('passwordU'),
            'is_admin'  => false
        ]);
    }
}
