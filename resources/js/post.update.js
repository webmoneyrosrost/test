$(document).ready(function () {

    $('#postUpdateForm').on('submit', function () {

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if (data.status === 'success') {
                window.location = data.route;
            }
        }).fail(function(response) {
            let errors = response.responseJSON.errors;

            $("#errors-list").html("");
            if (errors.title) {
                $("#errors-list").append("<div class='alert alert-danger'>" + errors.title[0] + "</div>");
            }
            if (errors.description) {
                $("#errors-list").append("<div class='alert alert-danger'>" + errors.description[0] + "</div>");
            }
        });
        return false;

    });

});

