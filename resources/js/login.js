$(document).ready(function () {
    $('#loginAjax').on('submit', function () {

        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            if (data.status === 'success') {
                window.location = data.route;
            }
        }).fail(function(response) {
            let errorJson = JSON.parse(response.responseText);
            $("#errors-list").html("<div class='alert alert-danger'>" + errorJson.error + "</div>");
        });
        return false;

    });
});
