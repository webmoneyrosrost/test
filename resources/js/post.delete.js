$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.post-delete-form').on('click', function(e) {
        e.stopPropagation();
        e.preventDefault();

        let postURL = $(this).data('url'),
            tableRowObj = $(this);

        if (confirm("Are you sure you want to delete this post?")) {
            $.ajax({
                url: postURL,
                type: 'DELETE',
                dataType: 'json',
                success: function(data) {
                    alert(data.message);
                    tableRowObj.parents("tr").remove();
                }
            });
        }

    });

});

