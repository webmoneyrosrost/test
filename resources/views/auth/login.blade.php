@extends('layout')
@section('content')
<div class="login-form">
    <div class="logo">Login</div>
    <div class="login-item">

        <div id="errors-list"></div>

        <form action="{{ route('login-auth') }}" method="post" class="form form-login" id="loginAjax">
            @csrf
            <div class="form-field">
                <label class="user" for="login-username"><span class="hidden">Username</span></label>
                <input name="email" id="login-username" type="text" class="form-input" placeholder="Email" required>
            </div>

            <div class="form-field">
                <label class="lock" for="login-password"><span class="hidden">Password</span></label>
                <input name="password" id="login-password" type="password" class="form-input" placeholder="Password" required>
            </div>

            <div class="form-field">
                <input type="submit" value="Log in">
            </div>
        </form>
    </div>
</div>
@vite(['resources/js/login.js'])
@endsection
