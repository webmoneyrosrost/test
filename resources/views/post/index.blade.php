@extends('layout')
@section('content')

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Posts List</h2>
            </div>
            @can('create', \App\Models\Post::class)
                <div class="pull-right mb-2">
                    <a class="btn btn-success" href="{{ route('posts.create') }}"> Create Post</a>
                </div>
            @endcan
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>S.No</th>
            <th>Post Title</th>
            <th width="320px">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>

                    <a class="btn btn-success" href="{{ route('posts.show',$post->id) }}">Show</a>
                    @can('update', $post)
                        <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Edit</a>
                    @endcan
                    @can('delete', $post)
                        <a class="btn btn-danger post-delete-form" href="#" data-url="{{ route('posts.destroy',$post->id) }}">Delete</a>
                    @endcan

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @vite(['resources/js/post.delete.js'])
@endsection
